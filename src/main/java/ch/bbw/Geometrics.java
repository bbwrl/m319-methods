package ch.bbw;

public class Geometrics {

	public void greetings() {
		System.out.println("Hallo ich bin ein Objekt der Klasse Geometrics");
	}
	public void greetings(String name) {
		System.out.println(String.format("Hallo %s, willkommen, was kann ich für dich berechnen?",name));
	}
	public double rectangleArea(double width, double height) {
		return width * height;
	}
	public double circleArea(double radius) {
		return Math.PI * (radius * radius);
	}
	public double circleScope(double radius) {
		return Math.PI * radius * 2;
	}
	public double cylinderScope(double radius, double height) {
		return  2 * circleArea(radius) + rectangleArea(circleScope(radius), height);
	}

}
