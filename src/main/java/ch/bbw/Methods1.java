package ch.bbw;

public class Methods1 {
    public static void main(String[] args) {

        // Methoden 1
        Geometrics geometrics = new Geometrics();

        // Greetings
        geometrics.greetings();
        geometrics.greetings("Hugo");

        // Übung Rechtecksfläche
        double area1 = geometrics.rectangleArea(10.0, 20.0);
        System.out.println("Rechtecksfläche: " + area1);

        // Übung Kreisfläche
        double area2 = geometrics.circleArea(10);
        System.out.println("Kreisfläche: " + area2);

        // Übung Umfang
        double area3 = geometrics.circleScope(10);
        System.out.println("Kreisumfang: " + area3);

        // Übung Zylinder
        double area4 = geometrics.cylinderScope(3, 10);
        System.out.println(String.format("Zylinderfläche: %s", area4));
    }
}
