package ch.bbw;

public class FloorTiles {

    public double getSurface(double width, double height) {
        return width * height;
    }
    public int getNrOfTiles(double surface, double tileLength) {
        return (int)Math.round((surface / getSurface(tileLength, tileLength))+1);
    }
    public double getPrice(double surface, double pricePerSquareMeter) {
        return surface * pricePerSquareMeter;
    }
}
