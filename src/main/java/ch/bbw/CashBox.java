package ch.bbw;

import java.util.Scanner;

public class CashBox {

    public void coinReturn(double value) {
        int intValue = (int)(value*100);
        System.out.println("Für diesen Betrag benötigen Sie:");
        int fuenfliber = intValue / 500;
        intValue = intValue % 500;
        int zweifraenkler = intValue / 200;
        intValue = intValue % 200;
        int einfraenkler = intValue / 100;
        intValue = intValue % 100;
        int fuenfziger = intValue / 50;
        intValue = intValue % 50;
        int zwanziger = intValue / 20;
        intValue = intValue % 20;
        int zehner = intValue / 10;
        intValue = intValue % 10;
        int fuenfer = intValue / 5;
        intValue = intValue % 5;

        System.out.println("Fünfliber: " + fuenfliber);
        System.out.println("Zweifränkler: " + zweifraenkler);
        System.out.println("Einfränkler: " + einfraenkler);
        System.out.println("Fünfziger: " + fuenfziger);
        System.out.println("Zwanziger: " + zwanziger);
        System.out.println("Zehner:	" + zehner);
        System.out.println("Fünfer:	" + fuenfer);

    }

    public double difference(double value1, double value2) {
        return value1-value2;
    }

    public void printBill(double toPay, double payed) {
        System.out.println("Hier folgt der Kassenzettel");
    }
}
