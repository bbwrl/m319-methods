package ch.bbw;

import java.util.Scanner;

public class Methods2 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
/*
        System.out.println("Bitte geben Sie einen Betrag ein:");
        double value = keyboard.nextDouble();

        CashBox cashBox = new CashBox();
        System.out.println("Münzberechnung:");
        cashBox.coinReturn(value);

        double result = cashBox.difference(20, 18.75);
        System.out.println("Betrag 1: zu zahlender Betrag: " + 20);
        System.out.println("Betrag 2: eingeworfener Betrag: " + 18.75);
        System.out.println("Rückgabe: Differenz: " + result);
        cashBox.coinReturn(result);
*/
        System.out.println("Bodenplattenberechnung");
        FloorTiles floorTiles = new FloorTiles();

        System.out.println("Raumbreite in m:");
        double roomWidth = keyboard.nextDouble();
        System.out.println("Raumlänge in m:");
        double roomLength = keyboard.nextDouble();
        System.out.println("Seitenlänge der Bodenplatten (in m):");
        double tileLength = keyboard.nextDouble();

        double roomSurface = floorTiles.getSurface(roomWidth,roomLength);
        int nrOfTiles = floorTiles.getNrOfTiles(roomSurface,tileLength);

        System.out.println(String.format("Sie benötigen %d Bodenplatten", nrOfTiles));
        System.out.println(String.format("Die Raumfläche beträgt %.2f m2", roomSurface));

        double tilesSurface = nrOfTiles * tileLength*tileLength;
        System.out.println(String.format("Die Plattenfläche beträgt %.2f m2", tilesSurface));
        System.out.println(String.format("Der Verschnitt beträgt %.2f m2", tilesSurface-roomSurface));

        System.out.println("bei einem Gesamtpreis von Fr. " + floorTiles.getPrice(tilesSurface,155));

        keyboard.close();
    }
}
