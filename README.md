# Exercises with methods

## Content
- Coin return and tiles calculation
- and many others

## Clone the project
```bash
git clone https://gitlab.com/bbwrl/m319-methods.git
```

## contribute to the project
Teachers of the School BBW can
- contribute to the project by adding a pull request.
- contribute by writing a message to the author.

